package provider

import (
    "fmt"
    "github.com/gocolly/colly"
    "gitlab.com/iturricf/parrot/price"
    "net/http"
    "strconv"
    "strings"
    "time"
)

type mydProvider struct {
    Name string
    URL string
    Collector *colly.Collector
}

func NewMydProvider(collector *colly.Collector) *mydProvider {
    provider := new(mydProvider)

    provider.Name = "MyD Cambios"
    provider.URL = "https://www.mydcambios.com.py/"
    provider.Collector = collector

    return provider
}

func (provider *mydProvider) GetPrice() price.Price {
    pr := price.New()
    count := 0

    provider.Collector.OnHTML("div.cambios-banner > div > div > div:nth-child(1) div.cambios-banner-text", func (e *colly.HTMLElement) {
        var value string
        var pygToUsd float64
        var pygToArs float64
        var err error

        if (count > 0) {
            return
        }

        t := time.Now().UTC()

        value = e.ChildText("ul:nth-child(2) > li:nth-child(2)")
        value = strings.Replace(value, ",", "", -1)

        pygToUsd, err = strconv.ParseFloat(value, 64)

        if err != nil {
            fmt.Printf("Error parsing string value to float: %s\n", err.Error())
        }

        value = e.ChildText("ul:nth-child(3) > li:nth-child(3)")
        value = strings.Replace(value, ",", "", -1)

        pygToArs, err = strconv.ParseFloat(value, 64)

        pr.SetBid(pygToUsd / pygToArs)

        value = e.ChildText("ul:nth-child(2) > li:nth-child(3)")
        value = strings.Replace(value, ",", "", -1)

        pygToUsd, err = strconv.ParseFloat(value, 64)

        if err != nil {
            fmt.Printf("Error parsing string value to float: %s\n", err.Error())
        }

        value = e.ChildText("ul:nth-child(3) > li:nth-child(2)")
        value = strings.Replace(value, ",", "", -1)

        pygToArs, err = strconv.ParseFloat(value, 64)

        pr.SetAsk(pygToUsd / pygToArs)

        pr.SetExchange(provider.Name)
        pr.SetDate(t.Format(time.RFC3339))

        count += 1
    })

    cookie := new(http.Cookie)
    cookie.Name = "location"
    cookie.Value = "3"
    cookie.Path = "/"
    cookie.Domain = "www.mydcambios.com.py"

    err := provider.Collector.SetCookies(provider.URL, []*http.Cookie{cookie})

    if (err != nil) {
        fmt.Println("Error al asignar cookie", err.Error())
    }

    provider.Collector.Visit(provider.URL)

    return pr
}
