package provider

import (
    "fmt"
    "github.com/gocolly/colly"
    "gitlab.com/iturricf/parrot/price"
    "strconv"
    "strings"
    "time"
)

type bnaProvider struct {
    Name string
    URL string
    Collector *colly.Collector
}

func NewBNAProvider(collector *colly.Collector) *bnaProvider {
    provider := new(bnaProvider)

    provider.Name = "Banco Nación"
    provider.URL = "https://www.bna.com.ar"
    provider.Collector = collector

    return provider
}

func (provider *bnaProvider) GetPrice() price.Price {
    pr := price.New()

    provider.Collector.OnHTML("#billetes > table > tbody > tr:nth-child(1)", func (e *colly.HTMLElement) {
        var values []float64

        e.ForEach("td", func (_ int, el *colly.HTMLElement) {
            if (el.Text != "Dolar U.S.A") {
                v := strings.Replace(el.Text, ",", ".", -1)
                f, err := strconv.ParseFloat(v, 64)

                if err != nil {
                    fmt.Println("Error al parsear string: %s", err)
                }
                values = append(values, f)
            }
        })

        t := time.Now().UTC()

        pr.SetBid(values[0])
        pr.SetAsk(values[1])
        pr.SetExchange(provider.Name)
        pr.SetDate(t.Format(time.RFC3339))
    })

    provider.Collector.Visit(provider.URL)

    return pr
}
