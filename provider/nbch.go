package provider

import (
    "fmt"
    "github.com/gocolly/colly"
    "gitlab.com/iturricf/parrot/price"
    "strconv"
    "time"
)

type nbchProvider struct {
    Name string
    URL string
    Collector *colly.Collector
}

func NewNBCHProvider(collector *colly.Collector) *nbchProvider {
    provider := new(nbchProvider)

    provider.Name = "Banco del Chaco"
    provider.URL = "http://www.nbch.com.ar"
    provider.Collector = collector

    return provider
}

func (provider *nbchProvider) GetPrice() price.Price {
    pr := price.New()

    provider.Collector.OnHTML("div.home_contenedor_monedas > div.center > div > div:nth-child(3)", func (e *colly.HTMLElement) {
        bid, err := strconv.ParseFloat(e.ChildAttr("div > table > tbody > tr:nth-child(1) > td > span", "data-number"), 64)

        if err != nil {
            fmt.Printf("Error parsing string value to float: %s\n", err.Error())
        }

        ask, err := strconv.ParseFloat(e.ChildAttr("div > table > tbody > tr:nth-child(2) > td > span", "data-number"), 64)

        if err != nil {
            fmt.Printf("Error parsing string value to float: %s\n", err.Error())
        }

        t:= time.Now().UTC()

        pr.SetBid(bid)
        pr.SetAsk(ask)
        pr.SetExchange(provider.Name)
        pr.SetDate(t.Format(time.RFC3339))

    })

    provider.Collector.Visit(provider.URL)

    return pr
}

// #dnn_ctr424_HtmlModule_lblContent > div.backgroundImage21.home-fondo-monedasextranjeras > div.home_contenedor_monedas > div.center > div > div:nth-child(2) > div > div.home_cotizacion_con > table > tbody > tr:nth-child(1) > td.home_cotizacion_celda_number_compra.celdacentral_number_compra > span
// #dnn_ctr424_HtmlModule_lblContent > div.backgroundImage21.home-fondo-monedasextranjeras > div.home_contenedor_monedas > div.center > div > div:nth-child(3) > div > div.home_cotizacion_con > table > tbody > tr:nth-child(1) > td.home_cotizacion_celda_number_compra.celdacentral_number_compra > span
