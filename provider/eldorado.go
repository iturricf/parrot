package provider

import (
    "fmt"
    "github.com/gocolly/colly"
    "gitlab.com/iturricf/parrot/price"
    "strconv"
    "time"
)

type elDoradoProvider struct {
    Name string
    URL string
    Collector *colly.Collector
}

func NewElDoradoProvider(collector *colly.Collector) *elDoradoProvider {
    provider := new(elDoradoProvider)

    provider.Name = "El Dorado"
    provider.URL = "http://www.eldoradosa.com/cotizaciones/CotizacionesWeb.htm"
    provider.Collector = collector

    return provider
}

func (provider *elDoradoProvider) GetPrice() price.Price {
    pr := price.New()

    provider.Collector.OnHTML("table[width='100%']:nth-child(1)", func (e *colly.HTMLElement) {
        bid, err := strconv.ParseFloat(e.ChildText("tbody > tr:nth-child(1) > td:nth-child(3)"), 64)

        if err != nil {
            fmt.Printf("Error parsing string value to float: %s\n", err.Error())
        }

        ask, err := strconv.ParseFloat(e.ChildText("tbody > tr:nth-child(1) > td:nth-child(4)"), 64)

        if err != nil {
            fmt.Printf("Error parsing string value to float: %s\n", err.Error())
        }

        t:= time.Now().UTC()

        pr.SetBid(bid)
        pr.SetAsk(ask)
        pr.SetExchange(provider.Name)
        pr.SetDate(t.Format(time.RFC3339))

    })

    provider.Collector.Visit(provider.URL)

    return pr
}
