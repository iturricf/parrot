package provider

import "gitlab.com/iturricf/parrot/price"

type Provider interface {
    GetPrice() price.Price
}

var Providers = map[string]interface{} {
    "bna": NewBNAProvider,
    "nbch": NewNBCHProvider,
    "eldorado": NewElDoradoProvider,
    "myd": NewMydProvider,
}
