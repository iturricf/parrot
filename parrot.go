package main

import (
    "fmt"
    "github.com/gocolly/colly"
    "gitlab.com/iturricf/parrot/price"
    "gitlab.com/iturricf/parrot/provider"
    "reflect"
)

func main() {
    c := colly.NewCollector(
        colly.UserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.79 Safari/537.36"),
    )

    var prices []price.Price

    for _, v := range(provider.Providers) {
        fn := reflect.ValueOf(v)
        p := fn.Call([]reflect.Value{reflect.ValueOf(c)})
        prices = append(prices, p[0].Interface().(provider.Provider).GetPrice())
    }

    for _, v := range(prices) {
        fmt.Printf("COMPRA: %f - VENTA: %f - %s\n", v.GetBid(), v.GetAsk(), v.GetExchange())
    }
}
