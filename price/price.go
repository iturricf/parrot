package price

type price struct {
    bid float64
    ask float64
    date string
    exchange string
}

type Price interface {
    GetBid() float64
    GetAsk() float64
    GetExchange() string
    GetDate() string
    SetBid(bidPrice float64)
    SetAsk(askPrice float64)
    SetExchange(exchange string)
    SetDate(date string)
}

func New() *price {
    p := new(price)

    return p
}

func (p *price) GetBid() float64 {
    return p.bid
}

func (p *price) GetAsk() float64 {
    return p.ask
}

func (p *price) GetExchange() string {
    return p.exchange
}

func (p *price) GetDate() string {
    return p.date
}

func (p *price) SetBid(bidPrice float64) {
    p.bid = bidPrice
}

func (p *price) SetAsk(askPrice float64) {
    p.ask = askPrice
}

func (p *price) SetExchange(exchange string) {
    p.exchange = exchange
}

func (p *price) SetDate(priceDate string) {
    p.date = priceDate
}
